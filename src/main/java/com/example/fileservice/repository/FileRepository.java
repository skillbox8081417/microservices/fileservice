package com.example.fileservice.repository;

import com.amazonaws.services.s3.AmazonS3;
import com.example.fileservice.properties.S3Properties;
import org.springframework.stereotype.Component;

@Component
public class FileRepository extends S3Repository {
        public FileRepository(AmazonS3 s3Client, S3Properties properties) {
            super(s3Client, properties.getBucketPictures());
        }
}
