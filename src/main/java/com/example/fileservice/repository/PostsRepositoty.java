package com.example.fileservice.repository;

import com.example.fileservice.domain.Post;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PostsRepositoty extends JpaRepository<Post, Long> {

}
