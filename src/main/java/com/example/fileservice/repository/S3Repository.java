package com.example.fileservice.repository;

import com.amazonaws.AmazonServiceException;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.model.*;
import lombok.RequiredArgsConstructor;

import java.io.InputStream;
import java.util.Collection;
import java.util.Optional;

@RequiredArgsConstructor
public class S3Repository {
    private final AmazonS3 s3Client;
    private final String bucketName;

    public Collection<String> listKeys(String prefix) {
        return s3Client.listObjectsV2(bucketName, prefix)
                .getObjectSummaries().stream()
                .map(S3ObjectSummary::getKey)
                .toList();
    }

    public Collection<S3ObjectSummary> listObjects(String prefix) {
        return s3Client.listObjectsV2(bucketName, prefix)
                .getObjectSummaries();
    }

    public void delete(String key) {
        s3Client.deleteObject(bucketName, key);
    }

    public PutObjectResult put(String key, InputStream inputStream, ObjectMetadata metadata) {
        if (!s3Client.doesBucketExistV2(bucketName)){
           s3Client.createBucket(bucketName);
        }
        return s3Client.putObject(bucketName, key, inputStream, metadata);
    }

    public Optional<S3Object> get(String fileKey) {
        try {
            return Optional.of(s3Client.getObject(bucketName, fileKey));
        } catch (AmazonServiceException exception) {
            return Optional.empty();
        }
    }

    public void deleteBucket(String bucketName) {
        s3Client.deleteBucket(bucketName);
    }
}
