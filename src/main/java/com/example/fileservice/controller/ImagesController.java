package com.example.fileservice.controller;

import com.amazonaws.services.s3.model.S3ObjectInputStream;
import com.amazonaws.util.IOUtils;
import com.example.fileservice.service.ImagesService;
import io.swagger.v3.oas.annotations.Operation;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.io.InputStream;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;

@Slf4j
@RestController
@RequiredArgsConstructor
@RequestMapping(value = "/images")
public class ImagesController {
    private final ImagesService imagesService;

    @PostMapping(value = "/upload")
    @Operation(summary = "UploadFile")
    public String uploadImage(@RequestPart("image") MultipartFile image) throws IOException {
        String imageOriginalFilename = image.getOriginalFilename();
        log.info("S3 post event received for {}", imageOriginalFilename);
        return imagesService.uploadImage(imageOriginalFilename, image);
    }

    @DeleteMapping("/delete")
    @Operation(summary = "Delete the file")
    public String deleteImage(@RequestParam("fileName") String fileName) throws IOException {
        log.info("S3 remove event received for {}", fileName);
        return imagesService.deleteImage(fileName);
    }

    @GetMapping("getFile/{fileName}")
    public ResponseEntity<?> downloadFile(@PathVariable("fileName") String fileName) {
        var s3object = imagesService.getFile(fileName);
        S3ObjectInputStream inputStream = s3object.getObjectContent();
        byte[] content = null;
        try {
            content = IOUtils.toByteArray(inputStream);
            log.info("File downloaded successfully.");

        } catch (final IOException ex) {
            log.info("IO Error Message= " + ex.getMessage());
        }
        final ByteArrayResource resource = new ByteArrayResource(content);
        return ResponseEntity
                .ok()
                .contentLength(content.length)
                .header("Content-type", "application/octet-stream")
                .header("Content-disposition", "attachment; filename=\"" + fileName + "\"")
                .body(resource);
    }
}
