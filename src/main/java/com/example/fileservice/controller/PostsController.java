package com.example.fileservice.controller;

import com.example.fileservice.domain.Post;
import com.example.fileservice.dto.EventRecord;
import com.example.fileservice.producer.EventKafkaProducer;
import com.example.fileservice.service.ImagesService;
import com.example.fileservice.service.PostsService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.server.ResponseStatusException;

import java.io.IOException;
import java.time.LocalDateTime;
import java.util.UUID;
import java.util.concurrent.ExecutionException;

@Slf4j
@RestController
@RequestMapping("/posts")
@RequiredArgsConstructor
public class PostsController {
    private final PostsService postsService;
    private final ImagesService imagesService;
    private final EventKafkaProducer eventKafkaProducer;

    @GetMapping("/isPostExists/{id}")
    public boolean isPostExists(@PathVariable Long id) {
        return postsService.isPostExists(id);
    }

    @PostMapping("/createPost")
    public Post createPost (@RequestBody Post post, @RequestPart(value= "file") final MultipartFile multipartFile) throws IOException {
        imagesService.uploadImage(multipartFile.getOriginalFilename(), multipartFile);
        post.setFileName(multipartFile.getOriginalFilename());
        return  postsService.createPost(post);
    }

    @GetMapping("/getPost/{id}")
    public Post getPost (@PathVariable Long id) {
        return postsService.getPostById(id).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND));
    }

    @PutMapping("/{id}/like")
    public Post likePost(Long id) throws ExecutionException, InterruptedException {
        Post post = postsService.likePost(id);
        EventRecord event = EventRecord.builder()
                .eventInitiator(post.getUserId().toString())
                .uuid(UUID.fromString(post.getTitle()+post.getFileName()))
                .objectId(post.getId().toString())
                .objectType("Post")
                .dateTime(LocalDateTime.now())
                .build();
        eventKafkaProducer.sendMessage(event);
        return post;
    }
}
