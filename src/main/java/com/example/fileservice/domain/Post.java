package com.example.fileservice.domain;

import jakarta.persistence.*;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import java.time.LocalDateTime;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "posts")
public class Post {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "posts_generator")
    @SequenceGenerator(name="posts_generator", sequenceName = "posts_seq", allocationSize=1)
    private Long id;
    @NotBlank(message = "Please set up a field")
    @Length(max = 2048, message = "Cannot be longer than 2048")
    private String description;
    @Length(max = 255, message = "Cannot be longer than 255")
    private String title;
    private String fileName;
    @NotNull
    private LocalDateTime messageDate;
    private Long userId;
    private Long numberOfLikes;
}
