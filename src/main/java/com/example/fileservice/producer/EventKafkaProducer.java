package com.example.fileservice.producer;

import com.example.fileservice.dto.EventRecord;
import lombok.RequiredArgsConstructor;
import org.apache.kafka.clients.admin.NewTopic;
import org.apache.kafka.clients.producer.RecordMetadata;
import org.springframework.context.annotation.Bean;
import org.springframework.kafka.config.TopicBuilder;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;
import org.springframework.validation.annotation.Validated;

import java.util.concurrent.ExecutionException;

@Service
@RequiredArgsConstructor
public class EventKafkaProducer {
    private final KafkaTemplate<Object, Object> template;
    public static final String TOPIC = "statistics";

    @Bean
    public NewTopic topic() {
        return TopicBuilder
                .name(TOPIC)
                .partitions(1)
                .replicas(1)
                .build();
    }

    public String sendMessage(@Validated EventRecord event) throws ExecutionException, InterruptedException {
        RecordMetadata metadata = template
                .send(TOPIC, event)
                .get()
                .getRecordMetadata();
        return metadata.toString();
    }
}
