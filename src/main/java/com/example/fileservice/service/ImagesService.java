package com.example.fileservice.service;

import com.amazonaws.AmazonServiceException;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.S3Object;
import com.amazonaws.services.s3.model.S3ObjectInputStream;
import com.example.fileservice.properties.S3Properties;
import com.example.fileservice.repository.FileRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.FileUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.server.ResponseStatusException;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.Optional;

@Service
@Slf4j
@RequiredArgsConstructor
public class ImagesService {
    private static final String EVENT_DELETE = "s3:ObjectRemoved:";
    private static final String EVENT_CREATE = "s3:ObjectCreated:";
    @Autowired
    private final S3Properties properties;
    @Autowired
    private final FileRepository imagesRepository;


    public String uploadImage(String fileName, MultipartFile file) throws IOException {
        log.info("Uploading {} to bucket {} in S3", fileName ,properties.getBucketPictures());
        try (InputStream stream = new ByteArrayInputStream(file.getBytes())){
            ObjectMetadata metadata = new ObjectMetadata();
            metadata.setContentType(MediaType.MULTIPART_FORM_DATA_VALUE);
            metadata.setContentLength(file.getSize());
            imagesRepository.put(fileName, stream, metadata);
        } catch (AmazonServiceException e) {
            log.error(e.getErrorMessage());
            return String.format("Photo '%s' have not uploaded", fileName);
        }
        log.info("Photo '{}' was successfully uploaded", fileName);
        return String.format("Photo '%s' was successfully uploaded", fileName);
    }

    public String deleteImage(String fileName) {
        String bucketName = properties.getBucketPictures();
        log.info("Removing {} from bucket {} in S3", fileName, bucketName);
        if (imagesRepository.listKeys(fileName).isEmpty()) {
            return String.format("Photo '%s' have not deleted", fileName);
        }
        imagesRepository.delete(fileName);

        log.info("Photo '{}' was successfully deleted", fileName);
        return String.format("Photo '%s' was successfully deleted", fileName);
    }

    public S3Object getFile (String fileName) throws ResponseStatusException {
        return imagesRepository.get(fileName).orElseThrow(() -> new ResponseStatusException(
                HttpStatus.NOT_FOUND, "File with name: " + fileName + "not found in the repository"));

    }
}
