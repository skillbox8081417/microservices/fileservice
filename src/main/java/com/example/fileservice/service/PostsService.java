package com.example.fileservice.service;

import com.example.fileservice.domain.Post;
import com.example.fileservice.exception.ResourceNotFoundException;
import com.example.fileservice.properties.S3Properties;
import com.example.fileservice.repository.FileRepository;
import com.example.fileservice.repository.PostsRepositoty;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.util.Optional;

@Slf4j
@Service
@RequiredArgsConstructor
public class PostsService {

    private final PostsRepositoty postsRepo;
    private final ImagesService imagesService;

    public Post createPost(Post post) {
        return postsRepo.save(post);
    }

    public Optional<Post> getPostById(Long postId) {
        return postsRepo.findById(postId);
    }

    public boolean isPostExists(Long postId) {
        return postsRepo.existsById(postId);
    }

    public Post likePost(Long postId) {
        Post post = postsRepo.findById(postId).orElseThrow(() -> new ResponseStatusException(
                HttpStatus.NOT_FOUND, "Post with id: " + postId + "not found"));
        post.setNumberOfLikes(post.getNumberOfLikes()+1);
        return postsRepo.save(post);
    }
}
