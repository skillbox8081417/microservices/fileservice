create table if not exists posts (
	id int8 not null default nextval('posts_seq'),
	message_date timestamp without time zone not null,
	title varchar(255) not null,
	description varchar (2048),
	file_name varchar(2048),
	user_id int8,
	number_of_likes int8,
	constraint "posts_pk" primary key (id),
	unique (message_date,title,user_id)
);
CREATE unique index if not exists posts_unq_idx ON posts(message_date,title,user_id);
