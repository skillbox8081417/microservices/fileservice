package com.example.fileservice.service;

import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.model.ListObjectsV2Result;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.PutObjectResult;
import com.amazonaws.services.s3.model.S3Object;
import com.example.fileservice.base.BaseTest;
import com.example.fileservice.properties.S3Properties;
import com.example.fileservice.repository.FileRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;

import java.io.ByteArrayInputStream;
import java.io.File;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;

@EnableConfigurationProperties
@SpringBootTest
public class ImageServiceTest extends BaseTest {
        @Mock
        private AmazonS3 s3Client;
        private FileRepository fileRepository;
        @Autowired
        private S3Properties s3Properties;

        private final String AWS_BUCKET = "pictures-bucket";
        private final String KEY_NAME = "picture.jpg";

        @BeforeEach
        public void setup() {
            MockitoAnnotations.openMocks(this);
            fileRepository = new FileRepository(s3Client,s3Properties);
        }

    @Test
    void whenVerifyingListBuckets_thenCorrect() {
        ListObjectsV2Result result = new ListObjectsV2Result();
        result.setBucketName(AWS_BUCKET);
        result.setKeyCount(1);
        result.setPrefix(KEY_NAME);
        when(s3Client.listObjectsV2(AWS_BUCKET,KEY_NAME)).thenReturn(result);
        fileRepository.listKeys(KEY_NAME);
        verify(s3Client).listObjectsV2(AWS_BUCKET,KEY_NAME);
    }

    @Test
    void whenDeletingBucket_thenCorrect() {
        fileRepository.deleteBucket(AWS_BUCKET);
        verify(s3Client).deleteBucket(AWS_BUCKET);
    }

    @Test
    void whenVerifyingPutObject_thenCorrect() {
        File mockFile = mock(File.class);
        when(mockFile.length()).thenReturn(100L);
        when(mockFile.getName()).thenReturn(KEY_NAME);
        when(mockFile.getPath()).thenReturn("/uploads");
        PutObjectResult result = mock(PutObjectResult.class);

        ByteArrayInputStream stream = new ByteArrayInputStream(new byte[0]);
        ObjectMetadata metadata = new ObjectMetadata();
        metadata.setContentType(MediaType.MULTIPART_FORM_DATA_VALUE);
        metadata.setContentLength(mockFile.length());

        when(s3Client.putObject(anyString(), anyString(), any(), any())).thenReturn(result);

        assertThat(fileRepository.put(mockFile.getName(), stream, metadata)).isEqualTo(result);
        verify(s3Client).putObject(AWS_BUCKET, KEY_NAME, stream, metadata);
    }

    @Test
    void whenVerifyingListObjects_thenCorrect() {
        ListObjectsV2Result result = new ListObjectsV2Result();
        result.setBucketName(AWS_BUCKET);
        result.setKeyCount(1);
        result.setPrefix(KEY_NAME);
        when(s3Client.listObjectsV2(AWS_BUCKET,KEY_NAME)).thenReturn(result);
        fileRepository.listObjects(KEY_NAME);
        verify(s3Client).listObjectsV2(AWS_BUCKET, KEY_NAME);
    }

    @Test
    void whenVerifyingGetObject_thenCorrect() {
        when(s3Client.getObject(AWS_BUCKET,KEY_NAME)).thenReturn(new S3Object());
        fileRepository.get(KEY_NAME);
        verify(s3Client).getObject(AWS_BUCKET, KEY_NAME);
    }


    @Test
    void whenVerifyingDeleteObject_thenCorrect() {
        fileRepository.delete(KEY_NAME);
        verify(s3Client).deleteObject(AWS_BUCKET, KEY_NAME);
    }
}
